#pragma once

#include "alpha_code.hpp"
#include "country_codes.hpp"

#include <vector>

namespace isoc::mic {
namespace tag {
struct mic;
struct omic;
}  // namespace tag
using market_identifier_code = basic_alpha_code<4, tag::mic>;
using operating_market_identifier_code = basic_alpha_code<4, tag::omic>;
struct record {
  record(char const country[2],
         char const mic[4],
         char const omic[4],
         char const* name);

  ctc::alpha2_code country;
  market_identifier_code mic;
  operating_market_identifier_code operating_mic;
  char const* name;
};

std::vector<record> const& get_records();

record const& get_record(operating_market_identifier_code const&);
record const* find_record(market_identifier_code const&);
record const& get_record(market_identifier_code const&);
}  // namespace isoc::mic
