#pragma once

#include <format>

namespace isoc {

template<class Tag>
struct basic_numeric_code {
  explicit constexpr basic_numeric_code(int init)
    : value{init}
  {}
  explicit constexpr operator int() const { return value; }
  constexpr bool operator==(basic_numeric_code another) const
  {
    return value == another.value;
  }

private:
  int value;
};

}  // namespace isoc

template<class T>
struct std::formatter<isoc::basic_numeric_code<T>>: std::formatter<int> {
  template<typename Context>
  Context::iterator format(isoc::basic_numeric_code<T> const& num_code,
                           Context& ctx) const
  {
    return formatter<int>::format(static_cast<int>(num_code), ctx);
  }
};
