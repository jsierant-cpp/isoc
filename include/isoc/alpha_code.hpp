#pragma once

#include <cstdint>
#include <format>
#include <istream>
#include <iterator>
#include <ostream>
#include <stdexcept>
#include <string>

namespace isoc {
namespace detail {
template<std::size_t>
struct rep {
  using type = std::uint32_t;
};
template<>
struct rep<2> {
  using type = std::uint16_t;
};
}  // namespace detail

template<std::size_t size, class tag>
struct basic_alpha_code {
  static_assert(size == 2u || size == 3u || size == 4u);
  static bool is_convertible(std::string_view init)
  {
    return init.size() == size;
  }

  using value_type = typename detail::rep<size>::type;

  basic_alpha_code()
    : value{0}
  {}

  explicit basic_alpha_code(char const init[size])
    : value{0}
  {
    std::copy(&init[0], &init[size], &value.tab[0]);
  }

  explicit basic_alpha_code(std::string_view init)
    : value{0}
  {
    if (!is_convertible(init)) {
      throw std::invalid_argument{
        std::string{"alpha_code(): invalid character code size - "}
        + init.data()};
    }
    std::copy(std::begin(init), std::end(init), &value.tab[0]);
  }

  bool operator==(basic_alpha_code const& another) const noexcept
  {
    return value.integral == another.value.integral;
  }

  bool operator<(basic_alpha_code const& another) const noexcept
  {
    return value.integral < another.value.integral;
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  basic_alpha_code const& code)
  {
    os << code.value.tab[0] << code.value.tab[1];
    if constexpr (size > 2) { os << code.value.tab[2]; }
    if constexpr (size > 3) { os << code.value.tab[3]; }
    return os;
  }

  friend std::istream& operator>>(std::istream& is, basic_alpha_code& code)
  {
    is >> code.value.tab[0];
    is >> code.value.tab[1];
    if constexpr (size > 2) { is >> code.value.tab[2]; }
    if constexpr (size > 3) { is >> code.value.tab[3]; }
    return is;
  }

  friend std::string to_string(basic_alpha_code const& ac)
  {
    if constexpr (size == 4) {
      return {ac.value.tab[0],
              ac.value.tab[1],
              ac.value.tab[2],
              ac.value.tab[3]};
    }
    if constexpr (size == 3) {
      return {ac.value.tab[0], ac.value.tab[1], ac.value.tab[2]};
    }
    return {ac.value.tab[0], ac.value.tab[1]};
  }

  explicit constexpr operator value_type() const noexcept
  {
    return value.integral;
  }

private:
  union {
    value_type integral;
    char tab[size];
  } value;
};

}  // namespace isoc

namespace std {
template<size_t size, class T>
struct hash<::isoc::basic_alpha_code<size, T>>
  : private hash<typename ::isoc::basic_alpha_code<size, T>::value_type> {
  constexpr size_t operator()(::isoc::basic_alpha_code<size, T> const& ac) const
  {
    using value_type = typename ::isoc::basic_alpha_code<size, T>::value_type;
    return hash<value_type>::operator()(
      static_cast<typename ::isoc::basic_alpha_code<size, T>::value_type>(ac));
  }
};

template<size_t size, class U>
struct formatter<isoc::basic_alpha_code<size, U>, char>
  : formatter<std::string> {
  template<class Ctx>
  Ctx::iterator format(isoc::basic_alpha_code<size, U> const& v, Ctx& ctx) const
  {
    return formatter<std::string>::format(to_string(v), ctx);
  }
};

}  // namespace std
