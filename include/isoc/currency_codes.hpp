#pragma once

#include "alpha_code.hpp"
#include "country_codes.hpp"
#include "numeric_code.hpp"

#include <vector>

namespace isoc::crc {
namespace tag {
struct currency;
}
using alpha_code = basic_alpha_code<3, tag::currency>;
using numeric_code = basic_numeric_code<tag::currency>;

struct record {
  record(char const country_code[2],
         char const* name,
         char const alpha_code[],
         int num,
         int minor_unit);

  ctc::alpha2_code country;
  char const* name;
  crc::alpha_code alpha_code;
  numeric_code num_code;
  int minor_unit;
};

std::vector<record> const& get_records();

record const* find_record(alpha_code const& cc);
record const& get_record(alpha_code const& cc);
record const* find_record(ctc::alpha2_code const& cc);
record const& get_record(ctc::alpha2_code const& cc);

double round(double amount, alpha_code const& cc);

namespace literals {
alpha_code operator""_ac(char const* cc, std::size_t);
}

}  // namespace isoc::crc
