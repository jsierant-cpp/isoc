#pragma once

#include "alpha_code.hpp"
#include "numeric_code.hpp"

#include <vector>

namespace isoc::ctc {
namespace tag {
struct country;
};

using alpha2_code = basic_alpha_code<2, tag::country>;
using alpha3_code = basic_alpha_code<3, tag::country>;
using numeric_code = basic_numeric_code<tag::country>;

struct record {
  record(char const* n, char const a2c[], char const a3c[], int v);

  char const* name;
  alpha2_code a2code;
  alpha3_code a3code;
  numeric_code num_code;
};

std::vector<record> const& get_records();

record const* find_record(std::string const& name);
record const& get_record(std::string const& name);
record const* find_record(alpha2_code const&);
record const& get_record(alpha2_code const&);

namespace literals {
alpha2_code operator""_a2c(char const* cc, std::size_t);
alpha3_code operator""_a3c(char const* cc, std::size_t);
}  // namespace literals

}  // namespace isoc::ctc
