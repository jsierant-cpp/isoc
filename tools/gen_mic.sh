#!/bin/sh

DATA_CSV_FILE="$1"
OUT_CPP_FILE="$2"

NAMESPACE="isoc::mic"

echo "#include <isoc/market_identifier_codes.hpp>" > "$OUT_CPP_FILE"
echo "namespace $NAMESPACE {" >> "$OUT_CPP_FILE"
echo "std::vector<record> const& get_records() {" >> "$OUT_CPP_FILE"
echo "  static std::vector<record> records {" >> "$OUT_CPP_FILE"

cat "$DATA_CSV_FILE"\
  | cut -f2-6 -d';'\
  | tail +2\
  | sed -e 's/;[OS];/;/' -e 's/;/", "/g' -e 's/^/    { "/' -e 's/$/" },/' >> "$OUT_CPP_FILE"

echo "  };" >> "$OUT_CPP_FILE"
echo "  return records;" >> "$OUT_CPP_FILE"
echo "}" >> "$OUT_CPP_FILE"
echo "} // namespace $NAMESPACE" >> "$OUT_CPP_FILE"
