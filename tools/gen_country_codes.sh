#!/bin/sh

# data downloaded from https://www.iban.com/currency-codes

DATA_CSV_FILE="$1"
OUT_CPP_FILE="$2"

NAMESPACE="isoc::ctc"

echo "#include <isoc/country_codes.hpp>" > "$OUT_CPP_FILE"
echo "namespace $NAMESPACE {" >> "$OUT_CPP_FILE"
echo "std::vector<record> const& get_records() {" >> "$OUT_CPP_FILE"
echo "  static std::vector<record> records {" >> "$OUT_CPP_FILE"

cat "$1"\
  | tail +2\
  | sed -E 's/;/", "/g'\
  | sed -E 's/^/    { "/'\
  | sed -E 's/0+([1-9][0-9]*)/\1/g'\
  | sed -E 's/"([0-9]+)/\1/'\
  | sed -E 's/$/ },/'  >> "$OUT_CPP_FILE"

echo "  };" >> "$OUT_CPP_FILE"
echo "  return records;" >> "$OUT_CPP_FILE"
echo "}" >> "$OUT_CPP_FILE"
echo "} // namespace $NAMESPACE" >> "$OUT_CPP_FILE"
