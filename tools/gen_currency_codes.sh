#!/bin/sh

# based on https://www.six-group.com/en/products-services/financial-information/data-standards.html

DATA_CSV_FILE="$1"
DATA_COUNTRY_CODES="$2"
OUT_CPP_FILE="$3"

get_country_code() ( ctc_file="$1"; country_name="$2"
  country_name=$(echo "$country_name" | sed "s/\’/'/g" | sed 's/\[/\[/g' | sed 's/\]/\]/g')
  grep -i "^$country_name;" "$ctc_file" | cut -f2 -d';'
)

NAMESPACE="isoc::crc"
echo "#include <isoc/currency_codes.hpp>" > "$OUT_CPP_FILE"
echo "namespace $NAMESPACE {" >> "$OUT_CPP_FILE"
echo "std::vector<record> const& get_records() {" >> "$OUT_CPP_FILE"
echo "  static std::vector<record> records {" >> "$OUT_CPP_FILE"

grep -Ev 'No universal currency|US Dollar \(Next day\)|Mvdol;BOV|WIR Euro|WIR Franc' "$DATA_CSV_FILE"\
  | tail +2\
  | while IFS= read -r entry; do

  country_name=$(echo "$entry" | cut -f1 -d';')
  country_code=$(get_country_code "$2" "$country_name")

  if [ -z "$country_code" ]; then
    echo "Missing country code for $country_name"
    continue
  fi
  name=$(echo "$entry" | cut -f2 -d';')
  alpha_code=$(echo "$entry" | cut -f3 -d';')
  numeric_code=$(echo "$entry" | cut -f4 -d';' | sed -E 's/0+([1-9][0-9]*)/\1/')
  minor_unit=$(echo "$entry" | cut -f5 -d';')
  echo "    { \"$country_code\", \"$name\", \"$alpha_code\", $numeric_code, ${minor_unit}u }," >> "$OUT_CPP_FILE"
done

echo "  };" >> "$OUT_CPP_FILE"
echo "  return records;" >> "$OUT_CPP_FILE"
echo "}" >> "$OUT_CPP_FILE"
echo "} // namespace $NAMESPACE" >> "$OUT_CPP_FILE"
