#include "isoc/country_codes.hpp"

#include "utility.hpp"

namespace isoc::ctc {

namespace {
std::string get_name(record const& r) { return r.name; }

alpha2_code get_alpha2_code(record const& r) { return r.a2code; }
}  // namespace

record::record(char const* n, char const a2c[], char const a3c[], int v)
  : name{n}
  , a2code{a2c}
  , a3code{a3c}
  , num_code{v}
{}

record const* find_record(std::string const& name)
{
  static auto const listing = create_listing(&get_name, get_records());
  return isoc::find_record(name, listing);
}

record const& get_record(std::string const& name)
{
  return get_record_ref(name, find_record(name), "country");
}

record const* find_record(alpha2_code const& code)
{
  static auto const listing = create_listing(&get_alpha2_code, get_records());
  return isoc::find_record(code, listing);
}

record const& get_record(alpha2_code const& code)
{
  return get_record_ref(code, find_record(code), "country");
}

namespace literals {
alpha2_code operator""_a2c(char const* cc, std::size_t)
{
  return alpha2_code{cc};
}

alpha3_code operator""_a3c(char const* cc, std::size_t)
{
  return alpha3_code{cc};
}
}  // namespace literals

}  // namespace isoc::ctc
