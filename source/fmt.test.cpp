#include "isoc/numeric_code.hpp"
#include "isoc/alpha_code.hpp"

#include <boost/ut.hpp>

namespace isoc {
struct dummy_unit {};
using test_ac = basic_alpha_code<2, dummy_unit>;
using test_nc = basic_numeric_code<dummy_unit>;

boost::ut::suite<"[isoc::fmt]"> fmt_suite = [] {
  using namespace boost::ut;
  "alpha code"_test = [] { expect(std::format("{}", test_ac{"AB"}) == "AB"); };
  "numeric code"_test = [] { expect(std::format("{}", test_nc{2}) == "2"); };
};

}  // namespace strict

