#pragma once

#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

namespace isoc {
inline std::string to_string(std::string const& s) { return s; }

template<class R, class F>
auto create_listing(F&& get_key, std::vector<R> const& records)
{
  using Key = decltype(get_key(std::declval<R const&>()));
  std::unordered_map<Key, std::reference_wrapper<R const>> result;
  for (auto const& r : records) { result.emplace(get_key(r), r); }
  return result;
}

template<class R, class T>
R const* find_record(
  T const& value,
  std::unordered_map<T, std::reference_wrapper<R const>> const& listing)
{
  auto record = listing.find(value);
  if (record == std::cend(listing)) { return nullptr; }
  return std::addressof(record->second.get());
}

template<class R, class T>
R const& get_record_ref(T const& value,
                        R const* record,
                        std::string const& name)
{
  if (record == nullptr) {
    throw std::out_of_range{"unknown " + name + " " + to_string(value)};
  }
  return *record;
}

template<class R, class T>
R const& get_record(
  T const& value,
  std::unordered_map<T, std::reference_wrapper<R const>> const& listing,
  std::string const& name)
{
  return get_record_ref(value, find_record(value, listing), name);
}
}  // namespace isoc
