#include "isoc/currency_codes.hpp"

#include "utility.hpp"

#include <cmath>

namespace isoc::crc {
namespace {
ctc::alpha2_code get_country(record const& r) { return r.country; }

alpha_code get_alpha_code(record const& r) { return r.alpha_code; }
}  // namespace

record::record(char const c[2],
               char const* n,
               char const ac[3],
               int num,
               int mu)
  : country{c}
  , name{n}
  , alpha_code{ac}
  , num_code{num}
  , minor_unit{mu}
{}

record const* find_record(alpha_code const& code)
{
  static auto const listing = create_listing(&get_alpha_code, get_records());
  return isoc::find_record(code, listing);
}

record const& get_record(alpha_code const& code)
{
  return get_record_ref(code, find_record(code), "currency");
}

record const* find_record(ctc::alpha2_code const& code)
{
  static auto const listing = create_listing(&get_country, get_records());
  return isoc::find_record(code, listing);
}

record const& get_record(ctc::alpha2_code const& code)
{
  return get_record_ref(code, find_record(code), "country");
}

double round(double amount, alpha_code const& cc)
{
  auto const& record = get_record(cc);
  auto unit_shift = std::pow(10, record.minor_unit);
  return std::round(amount * unit_shift) / unit_shift;
}

namespace literals {
alpha_code operator""_ac(char const* cc, std::size_t) { return alpha_code{cc}; }
}  // namespace literals

}  // namespace isoc::crc
