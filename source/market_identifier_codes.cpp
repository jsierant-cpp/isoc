#include "isoc/market_identifier_codes.hpp"

#include "utility.hpp"

namespace isoc::mic {
namespace {
operating_market_identifier_code get_omic(record const& r)
{
  return r.operating_mic;
}

market_identifier_code get_mic(record const& r) { return r.mic; }
}  // namespace

record::record(char const market_country[2],
               char const market_mic[4],
               char const oper_mic[4],
               char const* market_name)
  : country{market_country}
  , mic{market_mic}
  , operating_mic{oper_mic}
  , name{market_name}
{}

record const& get_record(operating_market_identifier_code const& omic)
{
  static auto const listing = create_listing(&get_omic, get_records());
  return get_record(omic, listing, "OMIC");
}

record const* find_record(market_identifier_code const& mic)
{
  static auto const listing = create_listing(&get_mic, get_records());
  return isoc::find_record(mic, listing);
}

record const& get_record(market_identifier_code const& mic)
{
  return get_record_ref(mic, find_record(mic), "MIC");
}
}  // namespace isoc::mic
